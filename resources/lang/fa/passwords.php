<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'پسورد باید حداقل 6 کارکتر باشه و با تکرارش مطابقت داشته باشه.',
    'reset' => 'رمزتون عوض شد',
    'sent' => 'لینک تغییر رمز فرستاده شد.',
    'token' => 'This password reset token is invalid.',
    'user' => "این ایمیل رو نداریم. ",

];
