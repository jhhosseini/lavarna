<?php
return [
		'wrongAnswer' => 'راز (پاسخ) وارد شده اشتباه بود.',
		'correctAnswer' => 'احسنت به تو! پاسخ صحیح بود!',
		'downloadAttachment'=> 'دریافت فایل پیوست این سوال',
		'threadsFinished'=> 'نخ بعدی وجود ندارد، تمام شد',
		'prevThreadFinished'=> 'نخ قبلی تمام شد! آفرن! ',
		'waitFor'=> 'از آخرین پاسخ اشتباه شما برای این سوال باید انقدر بگذرد'
];