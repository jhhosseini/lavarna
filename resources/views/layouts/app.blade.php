<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lavarna - @yield('title')</title>

        <!-- CSS And JavaScript -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/lavarna.css" rel="stylesheet">
<!--         <link href='http://awebfont.ir/css?id=1554' rel='stylesheet' type='text/css'> -->
        <link href="/css/bootstrap-rtl.min.css" rel="stylesheet">
        
        <!-- Custom styles for this template -->
   		<link href="/css/navbar.css" rel="stylesheet">
   		
   		
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	   		
        	<script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="/assets/js/MathJax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script type="text/x-mathjax-config">
		  MathJax.Hub.Config({showMathMenu:false,tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
		
	</script>
        
    </head>

    <body>
    <div id="background">
	@include('common.navbar')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">@yield('title')
				</div>

				<div class="panel-body">
				@include('common.allmsgs')
					<!-- Display Validation Errors -->
					@yield('content')
				</div>
			</div>
		</div>
			<div class="col-sm-offset-2 col-sm-8">
		<audio controls="">
		  <source src="/lavarna.mp3" type="audio/mpeg">
		</audio>
		</div>
	</div>


</div>



    <script type="text/javascript">
      $('button').popover();
    </script>
    </body>
</html>