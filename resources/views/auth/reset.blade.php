@extends('layouts.app') @section('content')

@section('title',trans('labels.changePassword'))

<form method="POST" action="/password/reset" class="form-horizontal">
	{!! csrf_field() !!} <input type="hidden" name="token"
		value="{{ $token }}">

	<div class="form-group">
		<label for="email" class="col-sm-3 control-label">{{
			trans('labels.Email') }}</label>

		<div class="col-sm-6">
			<input type="email" name="email" class="form-control"
				value="{{ old('email') }}">
		</div>
	</div>
	<!-- Password -->
	<div class="form-group">
		<label for="password" class="col-sm-3 control-label">{{
			trans('labels.newPassword') }}</label>

		<div class="col-sm-6">
			<input type="password" name="password" class="form-control">
		</div>
	</div>

	<!-- Confirm Password -->
	<div class="form-group">
		<label for="password_confirmation" class="col-sm-3 control-label">{{trans('labels.ConfirmPassword')}}
		</label>

		<div class="col-sm-6">
			<input type="password" name="password_confirmation"
				class="form-control">
		</div>
	</div>

	<!-- Register Button -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-6">
			<button type="submit" class="btn btn-default">
				<i class="fa fa-btn fa-sign-in"></i>{{ trans('labels.Register') }}
			</button>
		</div>
	</div>
</form>

@endsection
