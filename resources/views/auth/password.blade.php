 @extends('layouts.app') @section('content')

<!-- todo: make the title assignable by the controller -->
@section('title',trans('labels.forgotPassword'))



<form method="POST" class="form-horizontal" action="/password/email">
	{{ csrf_field() }}

	
	<!-- Email Entry-->
	<div class="form-group">
		<label for="email" class="col-sm-3 control-label">{{
			trans('labels.Email') }}</label>

		<div class="col-sm-6">
			<input type="email" name="email" class="form-control"
				value="{{ old('email') }}">
		</div>
	</div>
	
	<!-- Captcha -->
	<div class="form-group">
		<label for="captcha" class="col-sm-3 control-label">{{
			trans('labels.Captcha') }} </label>

		<div class="col-sm-6">
			<p> <?php echo captcha_img(); ?> </p>
			<p>
				<input type="text" name="captcha">
			</p>

		</div>
	</div>

	<!-- Submit Button -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-7">
			<button type="submit" class="btn btn-default">
				<i class="fa fa-btn fa-sign-in"></i>{{ trans('labels.Submit') }}
			</button>
		</div>
	</div>

</form>
@endsection
