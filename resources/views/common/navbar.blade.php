<nav class="navbar navbar-default navbar-static-top">
	<div class="container container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				data-original-title="" title="">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			@if(!Auth::check())
				<a class="navbar-brand" href="/">{{trans('labels.appName')}}</a>
			@else
				<a class="navbar-brand" href="/"><?php echo Auth::user()->name;?>, {{trans('labels.welcome')}}</a>
			@endif
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				@can('gameDesign')
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false">{{trans('labels.gameDesign')}}
						<span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/question/new">{{trans('labels.addQuestion')}}</a></li>
						<li><a href="/questions">{{trans('labels.questionList')}}</a></li>
						<li class="divider"></li>
						<li><a href="/thread/new">{{trans('labels.addThread')}}</a></li>
						<li><a href="/threads">{{trans('labels.threadList')}}</a></li>
					</ul></li>
				@endcan
				@if(isset($question))
					@include('game.questionNav')
				@endif
			</ul>
			
			<ul class="nav navbar-nav navbar-right flip">
				 @if(!Auth::check())
				<li><a href="/auth/login">{{trans('labels.login')}}</a></li>
				<li><a href="/auth/register">{{trans('labels.register')}} </a></li>
				<li><a href="/password/email">{{trans('labels.forgotPassword')}} </a></li>
				@else
				<li><a href="/auth/logout">خروج</a></li>
				@endif
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>