<li class="dropdown"><a href="#" class="dropdown-toggle"
	data-toggle="dropdown" role="button" aria-expanded="false">{{trans('labels.Thread')}}
	<span class="badge">{{$question->thread->title}}</span>
		<span class="caret"></span>
</a>
	<ul class="dropdown-menu" role="menu">
		@foreach($question->thread->getSuperParents() as $parent)
				@include('game.questionNav.menuItem',['activeQstn'=>$question->thread->firstQuestion(),'question'=>$parent->firstQuestion(),'title'=>$parent->title])
			
			@for($kid=$parent->kid();$kid!=null;$kid=$kid->kid())
				@include('game.questionNav.menuItem',['activeQstn'=>$question->thread->firstQuestion(),'question'=>$kid->firstQuestion(),'title'=>$kid->title])
			@endfor
		<li class="divider"></li>
		@endforeach
	</ul></li>
	
	
<li class="dropdown"><a href="#" class="dropdown-toggle"
	data-toggle="dropdown" role="button" aria-expanded="false">{{trans('labels.question')}} 
	<span class="badge">{{$question->title}}</span>
		<span class="caret"></span>
</a>
	<ul class="dropdown-menu" role="menu">
		@foreach($question->thread->questions as $threadQuestion)
				@include('game.questionNav.menuItem',['activeQstn'=>$question,'question'=>$threadQuestion])
		@endforeach
	</ul></li>
</ul>