<li class="@if(isset($activeQstn)) {{($activeQstn->id==$question->id) ? "active":""}} @endif">
<a
	@can('viewQuestion',$question)
		 href="/solve/{{$question->id}}"> <span class="glyphicon glyphicon-flag" aria-hidden="true"></span>
	@elseif($question->thread->isThreadClosed())
		> <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
	@else
		> <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
	@endcan
	{{$title or $question->title}}
</a></li>