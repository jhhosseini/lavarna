@extends('layouts.app') @section('content')

<!-- @todo: html title doesn't make sense to be static-->
@section('title',trans('labels.threadDetails'))



<!-- New Task Form -->
<form action="{{$action}}" method="POST" class="form-horizontal"
	id="newThreadForm">
	{{ csrf_field() }}

	<!-- Title -->
	<div class="form-group">
		<label for="title" class="col-sm-3 control-label">{{
			trans('labels.title') }}</label>

		<div class="col-sm-6">
			<input type="text" name="title" class="form-control"
				value="{{ $title  }}">
		</div>
	</div>

	<!-- Parent Thread -->
	<div class="form-group">
		<label for="parentThread" class="col-sm-3 control-label">{{
			trans('labels.parentThread') }}</label>

		<div class="col-sm-6">
			<select class="form-control custom-control" name="parent_thread_id"
				form="newThreadForm">
				@foreach ($threads as $thread)
					@if ($parent_thread_id==$thread->id)
						<option value="{{$thread->id}}" selected>{{$thread->title}}</option>
					@else
						<option value="{{$thread->id}}">{{$thread->title}}</option>
					@endif
					
				@endforeach
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="open_on" class="col-sm-3 control-label">{{
			trans('labels.openOn') }}</label>

		<div class="col-sm-8">
			<input name="open_on" value="{{ $open_on  }}" type="date" /> 

			
		</div>
	</div>
	<div class="form-group">
		<label for="close_on" class="col-sm-3 control-label">{{
			trans('labels.closeOn') }}</label>

		<div class="col-sm-8">
			<input name="close_on" value="{{ $close_on }}" type="date" />			

		</div>
	</div>


	<div class="form-group">
		<label for="captcha" class="col-sm-3 control-label">{{
			trans('labels.Captcha') }} </label>

		<div class="col-sm-6">
			<p> <?php echo captcha_img(); ?> </p>
			<p>
				<input type="text" name="captcha">
			</p>

		</div>
	</div>

	<!-- Register Button -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-6">
			<button type="submit" class="btn btn-default">
				<i class="fa fa-btn fa-sign-in"></i>{{ trans('labels.Submit') }}
			</button>
		</div>
	</div>
</form>

@endsection
