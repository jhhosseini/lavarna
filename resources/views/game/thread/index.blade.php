@extends('layouts.app') @section('content')

@section('title',trans('labels.threadList'))


<div class="table-responsive">
	<table class="table table-striped">

		<thead>
			<tr>
				<th>{{trans('labels.title')}}</th>
				<th>{{trans('labels.parentThread')}}</th>
				<th>{{trans('labels.openOn')}}</th>
				<th>{{trans('labels.closeOn')}}</th>
				<th>{{trans('labels.edit')}}</th>
				<th>{{trans('labels.delete')}}</th>
				<th>{{trans('labels.questions')}}</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($threads as $thread)
			<tr>
				<td>{{$thread->title}}</td>
				<td>{{$thread->parent}}</td>
				<td>{{$thread->open_on}}</td>
				<td>{{$thread->close_on}}</td>
				<td><a href="/thread/edit/{{$thread->id}}">{{trans('labels.edit')}}</a></td>
				<td><a href="/thread/destroy/{{$thread->id}}">{{trans('labels.delete')}}</a></td>
				<td><a href="/thread/solve/{{$thread->id}}">{{trans('labels.questions')}}</a></td>
			</tr>

			@endforeach
		</tbody>
	</table>
</div>
@endsection
