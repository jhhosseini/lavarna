@extends('layouts.app') @section('content')

<!-- todo: make the title assignable by the controller -->
@section('title',trans('labels.questionList'))


<div class="table-responsive">
<table class="table table-striped">

<thead>
<tr>
<th>{{trans('labels.id')}}</th>
<th>{{trans('labels.title')}}</th>
<th>{{trans('labels.Thread')}}</th>
<th>{{trans('labels.secret')}}</th>
<th>{{trans('labels.edit')}}</th>
<th>{{trans('labels.delete')}}</th>
</tr>
</thead>
<tbody>
@foreach ($questions as $question)
	<tr>
	<td>{{$question->id}}</td>
	<td>{{$question->title}}</td>
	<td><a href="/thread/edit/{{$question->thread_id}}">{{$question->thread->title}}</a></td>
	<td>{{$question->secret}}</td>
	<td><a href="/question/edit/{{$question->id}}">{{trans('labels.edit')}}</a></td>
	<td><a href="/question/destroy/{{$question->id}}">{{trans('labels.delete')}}</a></td>
	</tr>

	@endforeach
	</tbody>
	</table>
	</div>
	@endsection
