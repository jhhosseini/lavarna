@extends('layouts.app') @section('content') 

@section('title', $question->getTitle())


<!-- New Task Form -->
<form action="{{$question->getSolveUrl()}}" method="POST"
	" enctype="multipart/form-data" class="form-horizontal">
	{{ csrf_field() }}

	<!-- Body Appearance -->
	<div class="row" style="margin-bottom: 30px">
		<div id="MathPreview" class="col-sm-9">{{$question->body}}</div>
	</div>

	@if (($question->file_url)!='')
	<!-- File -->
	<div class="row" style="margin-bottom: 30px">
		<div class="col-sm-12">
			<div class="alert alert-info">
				<a href="/questionFile/{{$question->id}}">{{trans('messages.downloadAttachment')}}</a>
			</div>
		</div>
	</div>
	@endif

	<!-- secret -->
	<div class="form-group">
		<label for="secret" class="col-sm-3 control-label">{{
			trans('labels.secret') }}</label>

		<div class="col-sm-7">
			<input type="text" name="secret" class="form-control"> <input
				type="hidden" name="question_id" value="{{$question->id}}">
		</div>
	</div>

	<!-- Captcha -->
	<div class="form-group">
		<label for="captcha" class="col-sm-3 control-label">{{
			trans('labels.Captcha') }} </label>

		<div class="col-sm-7">
			<p> <?php echo captcha_img(); ?> </p>
			<p>
				<input type="text" name="captcha">
			</p>

		</div>
	</div>

	<!-- Submit Button -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-7">
			<button type="submit" class="btn btn-default">
				<i class="fa fa-btn fa-sign-in"></i>{{ trans('labels.Submit') }}
			</button>
		</div>
	</div>
</form>

@endsection

