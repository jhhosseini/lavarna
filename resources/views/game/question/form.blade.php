
@extends('layouts.app') @section('content')

@section('title', isset($htmlTitle) ? $htmlTitle : trans('labels.questionDetails'))



<!-- New Task Form -->
<form action="{{$action}}" method="POST" class="form-horizontal" enctype="multipart/form-data"
	id="newQuestionForm">
	{{ csrf_field() }}

	<!-- Title -->
	<div class="form-group">
		<label for="title" class="col-sm-3 control-label">
		{{trans('labels.title') }}</label>

		<div class="col-sm-7">
			<input type="text" name="title" class="form-control"
				value="{{ $title }}">
		</div>
	</div>

	<!-- MathJax Integration -->
			<script>
		$('ducument').ready(function(){
			 Preview = {
			  delay: 150,        // delay after keystroke before updating
			
			  preview: null,     // filled in by Init below
			  buffer: null,      // filled in by Init below
			
			  timeout: null,     // store setTimout id
			  mjRunning: false,  // true when MathJax is processing
			  oldText: null,     // used to check if an update is needed
			
			  //
			  //  Get the preview and buffer DIV's
			  //
			  Init: function () {
			    this.preview = document.getElementById("MathPreview");
			    this.buffer = document.getElementById("MathBuffer");
			  },
			
			  //
			  //  Switch the buffer and preview, and display the right one.
			  //  (We use visibility:hidden rather than display:none since
			  //  the results of running MathJax are more accurate that way.)
			  //
			  SwapBuffers: function () {
			    var buffer = this.preview, preview = this.buffer;
			    this.buffer = buffer; this.preview = preview;
			    buffer.style.visibility = "hidden"; buffer.style.position = "absolute";
			    preview.style.position = ""; preview.style.visibility = "";
			  },
			
			  //
			  //  This gets called when a key is pressed in the textarea.
			  //  We check if there is already a pending update and clear it if so.
			  //  Then set up an update to occur after a small delay (so if more keys
			  //    are pressed, the update won't occur until after there has been 
			  //    a pause in the typing).
			  //  The callback function is set up below, after the Preview object is set up.
			  //
			  Update: function () {
			    if (this.timeout) {clearTimeout(this.timeout)}
			    this.timeout = setTimeout(this.callback,this.delay);
			  },
			
			  //
			  //  Creates the preview and runs MathJax on it.
			  //  If MathJax is already trying to render the code, return
			  //  If the text hasn't changed, return
			  //  Otherwise, indicate that MathJax is running, and start the
			  //    typesetting.  After it is done, call PreviewDone.
			  //  
			  CreatePreview: function () {
			    Preview.timeout = null;
			    if (this.mjRunning) return;
			    var text = document.getElementById("MathInput").value;
			    if (text === this.oldtext) return;
			    this.buffer.innerHTML = this.oldtext = text;
			    this.mjRunning = true;
			    MathJax.Hub.Queue(
			      ["Typeset",MathJax.Hub,this.buffer],
			      ["PreviewDone",this]
			    );
			  },
			
			  //
			  //  Indicate that MathJax is no longer running,
			  //  and swap the buffers to show the results.
			  //
			  PreviewDone: function () {
			    this.mjRunning = false;
			    this.SwapBuffers();
			  }
			
			};
			
			//
			//  Cache a callback to the CreatePreview action
			//
			Preview.callback = MathJax.Callback(["CreatePreview",Preview]);
			Preview.callback.autoReset = true;  // make sure it can run more than once

			Preview.Init();
		});
		</script>
		
	
	<!-- Enter Body -->
	
	<div class="form-group">
		<label for="body" class="col-sm-3 control-label">{{	trans('labels.enterBody') }} </label>
		

		<div class="col-sm-7">
			<textarea style ="margin-bottom: 10px" id="MathInput" onkeyup="Preview.Update()" class="form-control custom-control" rows="3" name="body"
				form="newQuestionForm">{{ $body }}</textarea>
				<a  href="http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference">({{trans('labels.mathjaxGuide')}})</a>
		</div>
	</div>
	<!-- Body Appearance -->
	<div class="form-group">
		<label for="body" class="col-sm-3 control-label">{{	trans('labels.bodyAppearance') }}</label>

		<div id="MathPreview"class="col-sm-7">	</div>
		<div id="MathBuffer" class="col-sm-7" style="visibility:hidden; position:absolute; top:0; left: 0" >	</div>
	</div>
	<script>
// Preview.Init();
</script>

	<!-- File -->
	<div class="form-group">
		<label for="File" class="col-sm-3 control-label">{{trans('labels.file')}}
		</label>

		<div class="col-sm-7">
			@if (($file_url)!='')
				
				<a href="/questionFile/{{$id}}">{{trans('labels.currentFile')}}</a>
			@endif
			<input type="file" name="attachment" /> 
		</div>
	</div>

	<!-- secret -->
	<div class="form-group">
		<label for="secret" class="col-sm-3 control-label">{{
			trans('labels.secret') }}</label>

		<div class="col-sm-7">
			<input type="text" name="secret" class="form-control" value="{{$secret}}">
		</div>
	</div>

	<!-- Confirm Secret -->
	<div class="form-group">
		<label for="secret_confirmation" class="col-sm-3 control-label">{{trans('labels.confirmSecret')}}
		</label>

		<div class="col-sm-7">
			<input type="text" name="secret_confirmation" class="form-control">
		</div>
	</div>

	<!-- Thread -->
	<div class="form-group">
		<label for="thread_id" class="col-sm-3 control-label">{{
			trans('labels.Thread') }}</label>

		<div class="col-sm-7">
			<select class="form-control custom-control" name="thread_id"
				form="newQuestionForm">
				@foreach ($threads as $thread)
					@if ($thread_id==$thread->id)
						<option value="{{$thread->id}}" selected>{{$thread->title}}</option>
					@else
						<option value="{{$thread->id}}">{{$thread->title}}</option>
					@endif
					
				@endforeach
			</select>
		</div>
	</div>

	<!-- Parent Question -->
	<!-- 
	<div class="form-group">
		<label for="parentQuestion" class="col-sm-3 control-label">{{
			trans('labels.parentQuestion') }}</label>

		<div class="col-sm-7">
			<select class="form-control custom-control" name="parentQuestion"
				form="newQuestionForm">
				<option value="">parentQuestion</option>
				<option value="">parentQuestion</option>
				<option value="">parentQuestion</option>
				<option value="">parentQuestion</option>
			</select>
		</div>
	</div>
-->

	<div class="form-group">
		<label for="captcha" class="col-sm-3 control-label">{{
			trans('labels.Captcha') }} </label>

		<div class="col-sm-7">
			<p> <?php echo captcha_img(); ?> </p>
			<p>
				<input type="text" name="captcha">
			</p>

		</div>
	</div>

	<!-- Submit Button -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-7">
			<button type="submit" class="btn btn-default">
				<i class="fa fa-btn fa-sign-in"></i>{{ trans('labels.Submit') }}
			</button>
		</div>
	</div>
</form>

@endsection
