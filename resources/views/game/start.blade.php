@extends('layouts.app') @section('content')

@section('title', isset($htmlTitle) ? $htmlTitle : trans('labels.questionDetails'))

<br>
لطفا یک شاخه را انتخاب کنید
<br>
<br>
<div class="table-responsive">

	<table class="table table-striped">
		<thead>
			<tr>
				<th>{{trans('labels.title'  )}}</th>
				<th>{{trans('labels.openOn' )}}</th>
				<th>{{trans('labels.closeOn')}}</th>
				<th>{{trans('labels.start'  )}}</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($threads as $thread)
			<tr>
				<td>{{$thread->title}}</td>
				<td>{{$thread->jalali_open_on}}</td>
				<td>{{$thread->jalali_close_on}}</td>
				<td style="width:20%;">
				@can('startThread',$thread)
				<a class="btn btn-default btn-lg"href="{{$thread->questions->first()->getSolveUrl()}}">
				{{trans('labels.questions')}}</a>
				@endcan
				</td>
			</tr>

			@endforeach
		</tbody>
	</table>
</div>
@endsection
