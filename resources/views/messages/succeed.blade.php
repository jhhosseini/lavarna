@if(isset($strSuccesses))
	@if (count($strSuccesses) > 0)
		<!-- Form Error List -->
		<div class="alert alert-success ">
			<strong>خبرای خوب! خبرای خوب! </strong>
	
			<br><br>
	
			<ul>
				@foreach ($strSuccesses as $strError)
					<li>{{ $strError }}</li>
				@endforeach
			</ul>
		</div>
	@endif
@endif
