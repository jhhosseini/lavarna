@if(isset($strErrors))
	@if (count($strErrors) > 0)
		<!-- Form Error List -->
		<div class="alert alert-danger">
			<strong>نشد که بشه. یه مشکلی پیش اومد.</strong>
	
			<br><br>
	
			<ul>
				@foreach ($strErrors as $strError)
					<li>{{ $strError }}</li>
				@endforeach
			</ul>
		</div>
	@endif
@endif
