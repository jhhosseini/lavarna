<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Bigsinoos\JEloquent\PersianDateTrait;

class Thread extends Model 
{
	use PersianDateTrait;
    //
    private static $cachedThreads=null;
    
    public function questions() {
    	return $this->hasMany(Question::class);
    }
    
  	public function firstQuestion()
  	{
  		return $this->questions->first();
  	}
  	public function isThreadClosed()
  	{
  		return (time() < strtotime($this->open_on) || time() >strtotime($this->close_on));
  	}
    public function getSuperParents()
    {
    	return Thread::where('id','>','1')
						->where('parent_thread_id','=','1')
						->get();
    }
    public function getCachedThreads(){
		if(self::$cachedThreads==null)
		{
			self::$cachedThreads = Thread::all();
		}
		return self::$cachedThreads;
    }
    public function newInstanceValidations(){
		$editRules = $this->editInstanceValidations();
		unset($editRules['title']);
		return array_add($editRules,'title','required|min:3|unique:threads');
    }
    public function editInstanceValidations()
    {
    	return [
    	'captcha' => 'required|captcha',
    	'title'=>'required|min:3',
    	'parent_thread_id'=>'exists:threads,id',
    	'open_on'=>'required|date',
    	'close_on'=>'required|date',
    	];
    }
    
    public function ancestors()
    {
    	return ($this->ancestorsRecurse($this,[]));
    }
    
    /**
     * returns an array whose keys are the ids of the db row, in order to easily find the row.
     * @param unknown $dbThreads
     */
    public function cleanThreadsArray($dbThreads) {
    	$cleans = [];
    	
    	foreach($dbThreads as $dbThread)
    	{
    			$cleans[$dbThread->id]=$dbThread;		
    	}
    	return $cleans;
    }
    
    
    public function kid()
    {
    	$threads = $this->cleanThreadsArray($this->getCachedThreads());
    	foreach($threads as $thread)
    		if($thread->parent_thread_id==$this->id)
    			return $thread;
    	return null;
    }
    
    public function ancestorsRecurse($thread, $ancestorsList)
    {
    	$threads = $this->cleanThreadsArray($this->getCachedThreads());
    		
    	if($thread->id!=1)
    	{
    		array_push($ancestorsList, $threads[$thread->parent_thread_id]);
    		$ancestorsList = $this->ancestorsRecurse($threads[$thread->parent_thread_id], $ancestorsList);
    	}
		
    	return $ancestorsList;
    }
    
    public function populateFromRequest($request) {
    	$this->title            = $request->title;
    	$this->parent_thread_id = $request->parent_thread_id;
    	$this->open_on 	     	= $request->open_on .' 00:00:00';
    	$this->close_on         = $request->close_on.' 00:00:00';
    }
}
