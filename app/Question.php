<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * @todo: there is a design malfunction. if the question's answer changes the efforts
 * become meaningless, however the user should be able to continue the jouney
 * this case should be tested.
 */

class Question extends Model
{
    //
    public function user(){
    	return $this->belongsTo(User::class);
    }
    public function efforts(){
    	return $this->hasMany(Effort::class);
    }
    public function thread(){
    	return $this->belongsTo(Thread::class);
    }
    public function newInstanceValidations()
    {
    	return $this->editInstanceValidations();
    }
    public function editInstanceValidations()
    {
		return [
				'captcha' => 'required|captcha',
				'body' => 'required|min:10',
				'title' => 'required|min:3',
				'thread_id' => 'exists:threads,id',
				'user_id' => 'exists:users,id',
				'secret' => 'required|confirmed',
		];
		
	}
	
	public function populateFromRequest($request) {
		
		$this->body 		= $request->body 	   ;
		$this->title		= $request->title	   ;
		$this->thread_id	= $request->thread_id  ;
		$this->user_id		= $request->user()->id ;
		//@todo: use bcrypt(); here
		$this->secret		= trim($request->secret)  ;
		$this->save();
		
		if($request->hasFile('attachment'))
		{
			$file = $request->file('attachment');
			$name='ThreadID='.$this->thread_id.'----QuestionID='.$this->id;
			$file->move(app_path().'/ProtectedUploads/',$name.'.'.$file->getClientOriginalExtension());
			$fileUrl = app_path().'/ProtectedUploads/'.$name.'.'.$file->getClientOriginalExtension();
			$this->file_url = $fileUrl;
		}
	}
	public function getSolveUrl()
	{
		return '/solve/'.$this->id;
	}

	public function getTitle() {
// 		return $this->title;
		return $this->title.' ('.trans('labels.questionID').':'.$this->id.')';
	}
}
