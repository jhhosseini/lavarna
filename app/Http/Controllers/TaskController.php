<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use App\Task;
use App\User;
use App\Repositories\TaskRepository;

class TaskController extends Controller
{
    //
    private $taskRepository;
	
	public function __construct(TaskRepository $taskRepository)
	{
		$this->middleware('auth');
		
		$this->taskRepository = $taskRepository;
		
	}
	
	public function index(Request $request)
	{
		Storage::disk('local')->put('file.txt', 'Contents');
		
		return view('tasks.index',[
			'tasks' => $this->taskRepository->forUser($request->user())
		]);
	}
	
	public function uploadImage(Request $request)
	{
		var_dump($request->file("photo"));
		var_dump($_POST);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, array(
				'name'=>'required|max:255'
		));
		
		$request->user()->tasks()->create([
				'name'=> $request->name
		]);
		
		return redirect('/tasks');
	}

	public function destroy(Request $request, Task $task) {
		
		$this->authorize('destroy',$task);
		
		die(var_dump($task));
	}
}

