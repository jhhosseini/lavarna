<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Thread;
use Illuminate\View\View;
use Illuminate\Support\Facades\Schema;
use Hamcrest\Thingy;
use App\Question;
use Illuminate\Support\ViewErrorBag;

class ThreadController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
		$this->authorize('changeThread');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $threads = Thread::where('id','>','1')->get();
        foreach($threads as $thread)
        {
        	$parent = Thread::find($thread->parent_thread_id);
        	$thread->parent = $parent->title;
        }
        
        
        return view('game.thread.index',[
        		'threads' => $threads
        ]);
    }

    public function edit(Request $request,Thread $thread)
    {
    	$params = $thread->attributesToArray();
    	$params['open_on']=str_split($params['open_on'],10)[0];
    	$params['close_on']=str_split($params['close_on'],10)[0];
    	
    	// @todo: only the threads which are currently not the kids of this thread should be in this list
    	$params['threads']= $thread->getCachedThreads();
    	
    	$params = array_add($params, 'action','/thread/edit/'.$thread->id);
    	return view('game.thread.form',$params);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newForm(Request $request)
    {	
        //
        $thread = new Thread();
        $columns = Schema::getColumnListing('threads');
        
        $params =[];
        if(empty($request->old()))
        {
	        foreach($columns as $column)
	        {
	        	$params=array_add($params,$column,'');
	        }
        }
        else
        	$params = $request->old();
    	
        $params['threads']= $thread->getCachedThreads();
        $params = array_add($params,'action','/thread/new');
        return view('game.thread.form',$params);
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNew(Request $request)
    {
        //
        $thread = new Thread();
        $this->validate($request, $thread->newInstanceValidations());
        
        $thread->populateFromRequest($request);
        $thread->save();
        
        return redirect('/threads');
    }
    
    public function storeEdit(Request $request, Thread $thread)
    {
    	$this->validate($request, $thread->editInstanceValidations());
    
        $thread->populateFromRequest($request);
    	$thread->save();
    
    	return redirect('/threads');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @param int $id        	
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
    {
        //
    }
    
  
    //@todo: make a link to this somewhere
    public function threadQuestions(Request $request,Thread $thread)
    {
    	$questions = Question::where('thread_id',$thread->id)->get();
    	$title = trans('labels.questionsList').' '.$thread->title;
    	return view('game.question.index',['questions'=>$questions,'htmlTitle'=>$title]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Thread $thread)
    {
    	$thread->delete();
        return redirect('/threads');
        //
    }
}
