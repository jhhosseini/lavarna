<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;

use Mail;
use App\Http\Controllers\Controller;
use App\Thread;
use App\Question;
use App\Effort;

class PlayController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}
	public function solvePost(Request $request,Question $question)
	{
		
		if($request->user()->cannot('submitAnswer',$question))
		{
			return view ( 'game.question.solve' , [
					'strErrors' => [
							trans('messages.waitFor').' '.gmdate('H:i:s',$request->user()->shouldWaitForSubmit($question))
					],
					'question'=>$question,
	
			] );
		}
		$this->validate($request,[
    			'captcha'=>'required|captcha',
				'secret' => 'required'
	
		]);
		
		$effort 					= new Effort();
		$effort->user_id 			= $request->user()->id;
		$effort->proposed_answer 	= trim($request->secret);
		$effort->question_id		= $question->id;
		$effort->save();
		
	
		if($question->secret != $request->secret)
		{
			return view ( 'game.question.solve' , [
					'strErrors' => [
							trans('messages.wrongAnswer')
					],
					'question'=>$question,
	
			] );
		}
	
		$thread = $question->thread;
		$nextQuestion = Question::where('thread_id',$thread->id)
		->where('id','>',$request->question_id)
		->first();
		
		if(is_null($nextQuestion))
		{
			if ($thread->kid () != null)
				return redirect ( $thread->kid ()->questions->first()->getSolveUrl())->with ( 'messages', [ 
						'strSuccesses' => [ 
								trans ( 'messages.correctAnswer' ),
								trans ( 'messages.prevThreadFinished' ),
						]
						 
				] );
			;
	
			return view('layouts.app',[
					'strSuccesses'=> [
							trans('messages.threadsFinished')
					]
			]);
		}
		return redirect($nextQuestion->getSolveUrl())->with('messages',['strSuccesses'=>[	trans('messages.correctAnswer')	]]);
	
	}
	public function solveGet(Request $request ,Question $question)
	{
		$question->thread->getSuperParents();
		$this->authorize('viewQuestion',$question);
		$title = $question->title.' ('.trans('labels.questionID').':'.$question->id.')';
		$action = $question->getSolveUrl();
		
		$params = [
				'question'=>$question,
		];
		
		if($request->session()->has('messages'))
		{	
			$messages = $request->session()->get('messages');
			$params = array_merge($params, $messages);
		}
		
		return view('game.question.solve',$params);
	
	
	}

	public function start(Request $request){
		

// 		Mail::send('emails.password', ['token' => '123dasd13d23d'], function ($m) {
// 			$m->from('hello@app.com', 'Your Application');
		
// 			$m->to('j.hhoseini@gmail.com', 'participant')->subject('Your Reminder!');
// 		});
		
		$threads = new Thread();
		$threads = $threads->getSuperParents();

		foreach($threads as $thread)
		{
			
			$parent = Thread::find($thread->parent_thread_id);
			$thread->parent = $parent->title;
		}
		
		
		return view('game.start',[
				'threads' => $threads,
				'user'    => $request->user(),
		]);
	}
}
