<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Support\Facades\Schema;
use App\Thread;

class QuestionController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
		$this->authorize('changeQuestion');
	}	
	public function newForm(Request $request)
	{
		//
		$question = new Question();
		$columns = Schema::getColumnListing('questions');
	
		$params =[];
		foreach($columns as $column)
		{
			if(empty($request->old()->$column))
			{
				$params=array_add($params,$column,'');
			}
			else
				$params=array_add($params,$column,$request->old()->$column);
		}
			 
		$params['questions']= $question->all();
		$params['threads']  = Thread::where('id','>','1')->get();
		$params = array_add($params,'action','/question/new');
       	return view('game.question.form',$params);
	}

	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $params =['questions' => Question::all()];
        return view('game.question.index',$params);
    }

    public function file(Question $question)
    {
    	// this is not checked
    	$this->authorize('viewQuestion',$question);
    	return response()->download($question->file_url);
    }
    
    public function storeEdit(Request $request, Question $question)
    {
    	$this->validate($request, $question->editInstanceValidations());
    
    	$question->populateFromRequest($request);
    	$question->save();
    
    	return redirect('/questions');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeNew(Request $request)
    {
    	//
    	$question = new Question();
    	
    	$this->validate($request, $question->newInstanceValidations());
    
    	$question->populateFromRequest($request);
    	$question->save();
    
    	return redirect('/questions');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Question $question)
    {
    	// @todo: when validation error -> doesn't populate the form with the POSTed data, fix this.
    	$params = $question->attributesToArray();

    	$params['questions']= $question->all();
    	$params['threads']  = Thread::where('id','>','1')->get();
    	 
    	$params = array_add($params, 'action','/question/edit/'.$question->id);
    	return view('game.question.form',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    		
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Question $question)
    {
    	$question->delete();
        //
        
    	return redirect('/questions');
    }
}
