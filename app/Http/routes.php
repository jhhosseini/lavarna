<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Use App\Group;
Use App\User;
use PhpParser\Builder\Use_;



Route::get('/groups', function () {
	echo "groups";
	
	$groups= App\Group::all();
	var_dump($groups);
	foreach ($groups as $group)
		echo var_dump($group);
});
	
Route::get('/newgroup', function () {
	echo "adding one new group";
	
	$group = new Group;
	
	$group->class="113";
	$group->name="113Fighters";
	$group->password = md5("salam");
	
	var_dump($group);
	$group->save();
	
});
Route::get('/newUsers', function () {
	echo "adding 10 new users";
	
	for ($i = 0; $i < 10; $i++) {
		$user = new User();
		if($i<5){
			$user->name= "jay103";
			$user->email= $user->name+microtime();
			$user->group_id = 1;
			$user->save();
		}
		else{
			$user->name= "ali103";
			$user->email=$user->name+microtime();
			$user->group_id = 2;
			$user->save();
		}	
		var_dump($user);
	}
	
});

	Route::get('/users', function () {
		echo "users ";
		
		$users= App\User::all();
		//var_dump($users);
		foreach ($users as $user){
			$group = $user->group;
// 			var_dump($groups);
			echo $group->name;
// 			foreach($users as $user)
// 				echo $user->name.",,,".$user->user_id;
		}
		
	});
	
Route::get('/updateAllGroups', function () {
	echo "updating all group";

	$groups = Group::all();
	
	foreach($groups as $group){
		$group->class=106;
		$group->save();
		var_dump($group);
	}

// 	$group->class="113";
// 	$group->name="113Fighters";
// 	$group->password = md5("salam");

// 	var_dump($group);
// 	$group->save();

});
	// Password reset link request routes...
	Route::get('password/email', 'Auth\AuthController@getEmail');
	Route::post('password/email', 'Auth\AuthController@postEmail');
	
	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');
	
	// Authentication Routes...
	Route::get('auth/login', 'Auth\AuthController@getLogin');
	Route::post('auth/login', 'Auth\AuthController@postLogin');
	Route::get('auth/logout', 'Auth\AuthController@getLogout');
	
	// Registration Routes...
	Route::get('auth/register', 'Auth\AuthController@getRegister');
	Route::post('auth/register', 'Auth\AuthController@postRegister');
	
	// Registration Routes...
	Route::get('questionFile/{question}', 'Game\QuestionController@file');

	// Registration Routes...
	Route::get( 'solve/{question}', 'Game\PlayController@solveGet');
	Route::post('solve/{question}', 'Game\PlayController@solvePost');
	Route::any('/', 'Game\PlayController@start');
	
	// Registration Routes...
	Route::get('questions', 'Game\QuestionController@index');
	Route::get('question/new', 'Game\QuestionController@newForm');
	Route::post('question/new', 'Game\QuestionController@storeNew');
	Route::get('question/destroy/{question}', 'Game\QuestionController@destroy');
	Route::get('question/edit/{question}', 'Game\QuestionController@edit');
	Route::post('question/edit/{question}', 'Game\QuestionController@storeEdit');

	
	// Registration Routes...
	Route::get('thread/destroy/{thread}', 'Game\ThreadController@destroy');
	Route::get('thread/edit/{thread}', 'Game\ThreadController@edit');
	Route::post('thread/edit/{thread}', 'Game\ThreadController@storeEdit');
	Route::get('thread/new', 'Game\ThreadController@newForm');
	Route::post('thread/new', 'Game\ThreadController@storeNew');
	Route::any('threads', 'Game\ThreadController@index');

	Route::any('/tasks/uploadImage', 'TaskController@uploadImage');
	Route::get('/tasks', 'TaskController@index');
	Route::post('/task', 'TaskController@store');
	Route::delete('/task/{task}', 'TaskController@destroy');
	
	
