<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Effort;
use App\User;
use App\Question;
use Illuminate\Auth\Access\Gate;
use App\Thread;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Task' => 'App\Policies\TaskPolicy',
//     	Task::class => TaskPolicy::class,
    	
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
        
        $gate->before(function($user,$ability){
        	if($user->role===1)
        		return true;
        });
        $gate->define('startThread',function($user,$thread){
       		if($thread->isThreadClosed())
       			return false;
       		
        	if((count($thread->ancestors()))==1)
        		// the thread has no ancestor (the only one is the "without parent" ancestor) 
        		return true;

        		
        	$prevThreadLastQuestion = Thread::find($thread->parent_thread_id)->questions->last();
			        	
        	$succeededEffort = Effort::
        	where('user_id',$user->id)
        	->where('question_id',$prevThreadLastQuestion->id)
        	->where('proposed_answer',$prevThreadLastQuestion->secret)
        	->get()
        	->first();
			
	        return !is_null($succeededEffort);
        });
		
        $gate->define('viewQuestion', function (User $user,Question $question){
        	
       		if($question->thread->isThreadClosed())
       			return false;
        	
        	$prevQuestion = Question::
        	where('thread_id',$question->thread_id)
        	->where('id','<',$question->id)
        	->get()
        	->last();
        	
        	if(is_null($prevQuestion))
        	{
        		// the first question in the thread

        		
        		return $user->can('startThread',$question->thread);
        	}

        	$succeededEffort = Effort::where('user_id',$user->id)
        	->where('question_id',$prevQuestion->id)
        	->where('proposed_answer',$prevQuestion->secret)
        	->get()
        	->first();
			
	        return !is_null($succeededEffort);
        });
        
        $gate->define('submitAnswer', function (User $user,Question $question){
        
        	
        	return ($user->shouldWaitForSubmit($question)<0);
        });

        
        // if the following three checks are disabled nothing would change in the app, this is due to the "before" implementation
//         $gate->define('changeThread', function ($user){
//         	return $user->role==1;
//         });
        
//         $gate->define('changeQuestion', function ($user){
//         	return $user->role==1;
//         });
//         $gate->define('gameDesign', function ($user){
//         	return $user->role==1;
//         });
        //
    }
}
