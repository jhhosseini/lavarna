<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    
    public function lastEffort(Question $question){
    	 return Effort::where('user_id',$this->id)
    	->where('question_id',	  $question->id)
    	->get()
    	->last();
    }
    
    public function shouldWaitForSubmit($question)
    {
    	if(is_null($this->lastEffort($question)))
    		return -1;
    	$carbon = new Carbon();
    	$carbon = $this->lastEffort($question)->created_at;
    	$carbon->addMinutes(env('LAVARNA_WAIT_FOR_NEXT_ATTEMPT'));
    	return ($carbon->getTimestamp()-time());
    }
    public function group(){
    	return $this->belongsTo(Group::class);
    }

    public function efforts()
    {
    	return $this->hasMany(Effort::class);
    }
    public function  tasks()
    {
    	return $this->hasMany(Task::class);
    }
    
    public function questions()
    {
    	return $this->hasMany(Question::class);
    }
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}
